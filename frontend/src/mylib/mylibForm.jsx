import React from 'react'
import Grid from '../template/grid'
import IconButton from '../template/iconButton'

export default props => (
	<div role="form" className="bookForm">
		<Grid cols="12 9 10">
			<input id="title" className="form-control" placeholder="Título do livro"
													   onChange={props.handleChange}
													   value={props.title}></input>
		</Grid>
		<Grid cols="12 3 2">
			<IconButton style="primary" icon="plus" onClick={props.handleAdd}></IconButton>
		</Grid>
	</div>
)