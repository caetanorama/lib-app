import React, { Component } from 'react'
import axios from 'axios'

import PageHeader from '../template/pageHeader'
import BookForm from './mylibForm'
import BookList from './mylibBooks'


const URL = 'http://localhost:3003/api/mylib/'

export default class Mylib extends Component {
	constructor(props){
		super(props)
		this.state = { 	title: '', list: []	 }
		
		this.handleChange = this.handleChange.bind(this)
		this.handleAdd = this.handleAdd.bind(this)
	}
	handleChange(e) {
		this.setState({...this.state, title: e.target.value})
	}
	handleAdd() {
		const title = this.state.description
		axios.post(URL, { title }).then(resp => console.log('Funcionou'))
	}
	render() {
		return (
			<div>
			<PageHeader name="Biblioteca" small="Livros cadastrados"></PageHeader>
			<BookForm 
				title={this.state.title}
				handleChange={this.handleChange}
				handleAdd={this.handleAdd} />
			<BookList />
			</div>
		)
	}
}
