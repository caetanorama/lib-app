import React from 'react'
import {Link } from 'react-router-dom'


export default props => (
	<nav className="navbar navbar-inverse bg-inverse">
		<div className="container">
			<div className="navbar-header">
				<a className="navbar-brand" href="/">
					<i className="fa fa-book"></i> My Lib Manager
				</a>
			</div>
			<div className="navbar-collapse collapse" id="navbar">
				<ul className="nav navbar-nav">
					<li><Link to="/mylib">Biblioteca</Link></li>
					<li><Link to="/about">Sobre</Link></li>
				</ul>
			</div>
		</div>
	</nav>
)