import React, { Component } from 'react'
import {Route} from 'react-router-dom'

import Mylib from '../mylib/mylib'
import About from '../about/about'

const ROTAS  = [
	{path:'/', exact: true, renderComponent:() => <Mylib />},
	{path:'/mylib', exact: true, renderComponent:() => <Mylib />},
	{path:'/about', exact: true, renderComponent:() => <About />}
]

class Rotas extends Component {
	funcRotas() {
		return (
			ROTAS.map((i,index)=>(
				<Route key={index} path={i.path} exact={i.exact} component={i.renderComponent} />
			))
		)
	}
	
	render() {
		return (this.funcRotas())
	}
}
export default Rotas