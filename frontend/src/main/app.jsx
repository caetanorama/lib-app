import 'modules/bootstrap/dist/css/bootstrap.min.css'
import 'modules/font-awesome/css/font-awesome.min.css'

import React,{Component} from 'react'
import {BrowserRouter, Route, Link, Switch } from 'react-router-dom'

import Menu from '../template/menu'
import Rotas from './routes'

export default props => (
	<BrowserRouter >
	<div className="container">
		<Menu />
		<Rotas />
	</div>
	</BrowserRouter>
)