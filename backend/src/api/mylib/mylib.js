const restful = require('node-restful')
const mongoose = restful.mongoose

const bookSchema = new mongoose.Schema({
	title: {type: String, required: true},
	author: {type: String, required: true},
	publisher: {type: String, required: false},
	registerAt: {type: Date, default: Date.now}
})

module.exports = restful.model('Mylib',bookSchema)