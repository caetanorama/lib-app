const Mylib = require('./mylib')

Mylib.methods(['get','post','put','delete'])
Mylib.updateOptions({new: true, runValidators: true})

module.exports = Mylib 