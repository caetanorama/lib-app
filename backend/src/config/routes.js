const express = require('express')

module.exports = function(server){
	// API Routes
	const router = express.Router()
	server.use('/api',router)
	
	// Mylib routes
	const mylibService = require('../api/mylib/mylibservice')
	mylibService.register(router,'/mylib')
}